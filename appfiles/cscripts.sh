#!/bin/bash

# let's download the latest artifact from the gitlab master branch 
wget https://gitlab.com/ps.ali.alzubidy/devops-practices/-/jobs/artifacts/master/download?job=build_jar -O /apps/artifacts.zip

#time to unzip 
cd /apps && unzip -o artifacts.zip

# let's set few vars and stuff 
serverPort=8070

# let's print some info before running the app 
echo "Welcome to AZ Container Script"
echo "We're currently working inside "$(pwd)
echo "Server will be running on port :"$serverPort

# fireworks time
java -jar -Dserver.port=${serverPort} appfiles/target/*.jar


